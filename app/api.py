"""
REST API.
"""

from http import HTTPStatus

from flask import current_app, Flask, jsonify, request
from marshmallow import ValidationError
from sqlalchemy.exc import SQLAlchemyError

from app import db
from app.service import get_articles_with_keywords
from app.schemas import FindArticlesRequestSchema

app = Flask(__name__)


# noinspection PyUnusedLocal
@app.teardown_request
def remove_db_session(exception=None):
    db.session.remove()


@app.route('/articles/find', methods=['POST'])
def find_articles():
    """
    If query in request.json is not valid, returns HTTP 422.
    If query is valid, returns result with articles matching given keywords.
    """
    try:
        keywords = FindArticlesRequestSchema().load(request.json)
    except ValidationError as error:
        return jsonify(
            {
                "errors": error.messages
            }
        ), HTTPStatus.UNPROCESSABLE_ENTITY

    try:
        response = jsonify(
                {
                    "articles": [
                        {"text": i.header, "url": i.url} for i in get_articles_with_keywords(keywords)
                    ]
                }
            ), HTTPStatus.OK
    except SQLAlchemyError as error:
        response = jsonify(
            {
                "errors": "Internal server error. Please try again later."
            }
        ), HTTPStatus.INTERNAL_SERVER_ERROR
        current_app.logger.error(error)
    return response


if __name__ == "__main__":
    app.run(debug=True)
