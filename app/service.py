"""
Service layer.
"""
import logging
from typing import List

from sqlalchemy import any_

import app.db as db
from app.model import Article

logger = logging.getLogger(__name__)


def get_articles_with_keywords(keywords: List[str]) -> List[Article]:
    """
    Returns all articles from DB where at least one of given keywords is in article's header.
    The newest articles will be first. If no keywords were given, should return an empty list.
    """
    return db.session.query(Article).\
        filter(Article.header.ilike(
            any_(['%' + keyword + '%' for keyword in keywords])))\
        .order_by(Article.timestamp.desc())\
        .all()


def save_article_if_new(a: Article) -> None:
    """Saves an article, if it is not in our DB already. Existence is checked by URL of the article."""
    if db.session.query(Article.id).filter_by(url=a.url).count() == 0:
        db.session.add(Article(header=a.header, url=a.url))
        db.session.commit()
        logger.info(f"Added article: {a.url}")
