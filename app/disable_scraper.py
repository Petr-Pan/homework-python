"""
Script for removing scraper job from crontab.
"""
from crontab import CronTab

from app.scraper import job_command, get_job


if __name__ == '__main__':
    cron = CronTab(user=True)
    if job := get_job(cron, job_command):
        cron.remove(job)
        cron.write()
        print("Removed scraper job from crontab.")
    else:
        print("Scraper job not found in crontab. Nothing removed.")
