import re

import bs4
import pytest

import app.tests.data.news as test_data
from app.news import IdnesScraper, IhnedScraper, BbcScraper, NewsScraper


def check_provider(provider: NewsScraper):
    articles = provider.get_headers()
    assert len(articles) > 0
    for a in articles:
        assert len(a.header) > 0
        assert a.header.strip() == a.header
        assert re.fullmatch(r'http(s)?://.*', a.url)


def test_idnes():
    check_provider(IdnesScraper())


def test_ihned():
    check_provider(IhnedScraper())


def test_bbc():
    check_provider(BbcScraper())


@pytest.mark.parametrize(*test_data.extract_header)
def test_extract_header(scraper, raw_article, expected_header, comment):
    # setup
    soup = bs4.BeautifulSoup(raw_article, features='html.parser')
    # test
    assert scraper._extract_header(soup) == expected_header


@pytest.mark.parametrize(*test_data.extract_url)
def test_extract_url(scraper, raw_article, expected_url, comment):
    # setup
    soup = bs4.BeautifulSoup(raw_article, features='html.parser')
    # test
    assert scraper._extract_url(soup) == expected_url
