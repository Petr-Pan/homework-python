import pytest

import app.tests.data.api as test_data
from app import db
from app.api import app as flask_app
from app.model import Article


@pytest.fixture
def app():
    yield flask_app


@pytest.fixture
def client(app):
    return app.test_client()


def test_find_articles(app, client):
    # setup
    article = Article(header='test', url='https://test.com')
    db.create_empty_db()
    db.session.add(article)
    db.session.commit()
    response = client.post('/articles/find', json=dict(keywords=[article.header]))

    # test
    assert response.status_code == 200
    assert response.data.decode('utf-8').strip() == '{"articles":[{"text":"test","url":"https://test.com"}]}'


@pytest.mark.parametrize(*test_data.bad_request)
def test_bad_request(app, client, data, comment):
    # setup
    response = client.post('/articles/find', json=data)
    # test
    assert response.status_code == 422

# todo: add test that internal DB errors return 500 response, once a reliable way to create db fixtures is found
