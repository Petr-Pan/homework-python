bad_request = (
    ('data', 'comment'),
    [
        ({'kw': ['pes']}, 'invalid argument'),
        ({}, 'empty body'),
        ({'keywords': 'pes'}, 'invalid argument content'),
        ({'keywords': ['pes'], 'extra': 'kocka'}, 'extra argument'),
        ({'keywords': ['pes' for i in range(10000)]}, 'too large request')
    ])
