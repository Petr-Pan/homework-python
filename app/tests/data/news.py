import pytest

from app.news import IhnedScraper, IdnesScraper, BbcScraper


class Idnes:
    url = "https://www.idnes.cz/hokej/pohary/test"
    header = "ONLINE: Test"
    raw_article = f"""
        <a href="{url}" class="art-link">
        <h3>{header}
        </h3>
        <div class="art-img w230 ">
        <img alt="Test" src="//1gr.cz/fotky/idnes/20/122/w230/VP287fc47.JPG" title="Test." width="230" height="129">
        <span></span>
        </div>
        </a>
        <div class="art-info">
        <span class="time" datetime="2020-12-10T21:20:00" id="time-0">
        aktualizováno&nbsp;
        10. prosince 2020
        </span>
        </div>
        <p class="perex">
        <span class="brisk">Sledujeme online</span>
        Test
        </p>
    """


class Ihned:
    url = "https://archiv.ihned.cz/test-url"
    header = "ONLINE: Test"
    raw_article = f"""
        <article>
        <div class="article-media">
        <a href="https://test.ihned.cz/test">
        <img src="https://img.ihned.cz/attachment.php/610/75672610/ujGH0d4iy7I.jpg" alt=""></a></div>
        <div class="article-time"><span class="time">Před 24 min</span></div>
        <h2 class="article-title">
        <a href="{url}">{header}</a>
        </h2>
        <div class="article-author-time"><span class="time">Před 24 min</span></div>
        <div class="article-perex">
        <span class="lbl">Příběhy českých testů</span>
        <span>Když jeden z testů...</span>
        </div>
        </article>
    """


class Bbc:
    url = "https://www.bbc.com/sport/live/hockey/test"
    header = "ONLINE: Test"
    raw_article = f"""
        <div class="media block-link" data-bbc-container="sport" 
            data-bbc-title="Test preview: Team news and talking points" data-bbc-source="Hockey" 
            data-bbc-metadata="&quot;CHD&quot;: &quot;card::1&quot;">
        <div class="media__image">
        <div class="responsive-image">
        <img src="https://ichef.bbc.co.uk/wwhp/304/cpsprodpb/5A3F/production/test.jpg" 
            class="image-replace" alt="Split pic of test and test" 
            data-src="https://ichef.bbc.co.uk/wwhp/width/cpsprodpb/5A3F/production/test.jpg">
        </div></div>
        <div class="media__content">
        <h3 class="media__title">
        <a class="media__link" href="{url}" rev="sport|headline">
            {header}
        </a>
        </h3>
        <p class="media__summary">Coverage of Friday's Test</p>
        <a class="media__tag tag tag--sport" href="/sport/hockey" rev="sport|source">Hockey</a>
        </div>
        <a class="block-link__overlay-link" href="/sport/live/hockey/test" rev="sport|overlay"
            tabindex="-1" aria-hidden="true">
            Test preview: Team news and talking points
        </a>
        </div>
    """


extract_header = (
    ('scraper', 'raw_article', 'expected_header', 'comment'),
    [
        (IdnesScraper(), Idnes.raw_article, Idnes.header, 'Idnes'),
        (IhnedScraper(), Ihned.raw_article, Ihned.header, 'Ihned'),
        pytest.param(BbcScraper(), Bbc.raw_article, Bbc.header, 'BBC',
                     marks=pytest.mark.skip(reason="Not working - needs test case redesign "
                                                   "so it receives bs4.element.Tag"))
    ])

extract_url = (
    ('scraper', 'raw_article', 'expected_url', 'comment'),
    [
        (IdnesScraper(), Idnes.raw_article, Idnes.url, 'Idnes'),
        (IhnedScraper(), Ihned.raw_article, Ihned.url, 'Ihned'),
        (BbcScraper(), Bbc.raw_article, Bbc.url, 'BBC')
    ])
