import platform
from typing import List

import pytest
from crontab import CronTab

from app import scraper, db, news
from app.model import Article
from app.scraper import get_job


class FakeScraper(news.NewsScraper):
    def get_headers(self) -> List[news.Article]:
        return [news.Article(header='a', url='some-url'),
                news.Article(header='b', url='some-url')]


class FailingScraper(news.NewsScraper):
    def get_headers(self) -> List[news.Article]:
        raise Exception("Error when scraping")


def test_scrape_news():
    # mock news scrapers and clean DB
    scraper.SCRAPERS = [FakeScraper(), FailingScraper()]
    db.create_empty_db()

    # test
    scraper.scrape_news()

    # check
    articles = db.session.query(Article).all()
    assert len(articles) == 1
    article = articles[0]
    assert article.header == 'a'
    assert article.url == 'some-url'


@pytest.fixture()
def cron_fixture():
    cron = CronTab(user=True)
    command = f"echo homework news scraper - test cron"
    job = cron.new(command=command, comment='test')
    job.day.every(1)
    cron.write()

    yield dict(cron=cron, job=job, command=command)

    cron.remove(job)
    cron.write()


@pytest.mark.skipif(platform.system() != 'Linux',
                    reason='Only Linux platform is supported for scheduling.')
def test_get_job(cron_fixture):
    # test
    assert cron_fixture['job'] == get_job(cron_fixture['cron'], cron_fixture['command'])
