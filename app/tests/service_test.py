from app import db, service
from app.model import Article


def test_get_articles_with_keywords():
    # setup
    db.create_empty_db()
    db.session.add(Article(header='a b c', url=''))
    db.session.add(Article(header='b c d', url=''))
    db.session.add(Article(header='x y z', url=''))
    db.session.commit()

    # test
    assert not service.get_articles_with_keywords(keywords=['aaa'])

    assert service.get_articles_with_keywords(keywords=['a'])[0].header == 'a b c'
    assert service.get_articles_with_keywords(keywords=['d'])[0].header == 'b c d'
    assert service.get_articles_with_keywords(keywords=['y'])[0].header == 'x y z'

    assert 'a b c' in [a.header for a in service.get_articles_with_keywords(keywords=['b', 'c'])]
    assert 'b c d' in [a.header for a in service.get_articles_with_keywords(keywords=['b', 'c'])]


# todo: find how to create a db fixture that would allow multiple test cases in a single file
# def test_save_article_if_new():
#     # setup
#     db.create_empty_db()
#     expected_article = Article(header='test', url='test')
#     service.save_article_if_new(expected_article)
#
#     # test
#     article = service.get_articles_with_keywords(['test'])[0]
#     assert article.header == expected_article.header
#     assert article.url == expected_article.url
#
#
# def test_save_article_if_new_no_duplicates():
#     # setup
#     db.create_empty_db()
#     db.session.add(Article(header='test', url='test'))
#     db.session.commit()
#     service.save_article_if_new(Article(header='test', url='test'))
#
#     # test
#     assert len(service.get_articles_with_keywords(keywords=['test'])) == 1
