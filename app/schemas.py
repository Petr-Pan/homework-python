from marshmallow import fields, post_load, Schema
from marshmallow.validate import Length


class FindArticlesRequestSchema(Schema):
    keywords = fields.List(fields.Str(validate=Length(min=1, max=1000)),
                           required=True, validate=Length(max=1000))

    @post_load
    def create_keywords(self, data, **kwargs):
        return data['keywords']
