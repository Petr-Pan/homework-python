"""
News scrapers.
"""
from abc import abstractmethod
from dataclasses import dataclass
from typing import List

import bs4
import requests
import urllib.parse


@dataclass
class Article:
    """Represents an article from a news server."""
    header: str
    url: str


class NewsScraper:
    source_url = None
    base_url = None

    @abstractmethod
    def get_headers(self) -> List[Article]:
        """Returns a list of articles from the news server."""
        pass

    @abstractmethod
    def _extract_header(self, header: bs4.element.Tag) -> str:
        """Extracts header from a Tag element."""
        pass

    @abstractmethod
    def _extract_url(self, url: str) -> str:
        """
        Runs additional edits on URLs extracted by child classes.
        Child classes implement their own extraction of URLs from bs4.element.Tag
        and pass it to the parent.
        """
        return self._ensure_url_absolute(url)

    def _get_raw_articles(self, selector: str) -> bs4.ResultSet:
        """Returns a ResultSet of raw articles from source_url based on selector."""
        response = requests.get(self.source_url)
        soup = bs4.BeautifulSoup(response.text, features='html.parser')
        return soup.select(selector)

    def _ensure_url_absolute(self, url: str) -> str:
        """Prepends source_url of the news server if the supplied URL is relative"""
        return urllib.parse.urljoin(self.base_url, url)


class IdnesScraper(NewsScraper):
    source_url = 'https://www.idnes.cz/zpravy/archiv?idostrova=idnes'
    base_url = 'https://www.idnes.cz'

    def get_headers(self) -> List[Article]:
        raw_articles = self._get_raw_articles('div.art')

        return [Article(
                    header=self._extract_header(article),
                    url=self._extract_url(article)
                    )
                for article in raw_articles]

    def _extract_header(self, raw_article: bs4.element.Tag) -> str:
        return raw_article.select('h3')[0].text.strip()

    def _extract_url(self, raw_article: bs4.element.Tag) -> str:
        url = raw_article.select('a.art-link')[0].attrs.get('href')
        return super()._extract_url(url)


class IhnedScraper(NewsScraper):
    source_url = 'https://ihned.cz'
    base_url = source_url

    def get_headers(self) -> List[Article]:
        raw_articles = self._get_raw_articles('article')

        return [Article(
                    header=self._extract_header(article),
                    url=self._extract_url(article)
                    )
                for article in raw_articles]

    def _extract_header(self, raw_article: bs4.element.Tag) -> str:
        return raw_article.select('h2 a')[0].text.strip()

    def _extract_url(self, raw_article: bs4.element.Tag) -> str:
        url = raw_article.select('h2 a')[0].attrs.get('href')
        return super()._extract_url(url)


class BbcScraper(NewsScraper):
    source_url = 'https://www.bbc.com'
    base_url = source_url

    def get_headers(self) -> List[Article]:
        raw_articles = self._get_raw_articles('div.media')

        return [Article(
                    header=self._extract_header(article),
                    url=self._extract_url(article)
                    )
                for article in raw_articles]

    def _extract_header(self, raw_article: bs4.element.Tag) -> str:
        return raw_article.attrs.get('data-bbc-title').strip()

    def _extract_url(self, raw_article: bs4.element.Tag) -> str:
        url = raw_article.select('a')[0].attrs.get('href')
        return super()._extract_url(url)
