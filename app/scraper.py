"""
Web scraping service.
"""
import concurrent.futures
import logging
import platform
import sys
from logging.handlers import RotatingFileHandler
from pathlib import Path

from crontab import CronTab, CronItem

from requests.exceptions import RequestException

import app.service
from app.news import IdnesScraper, IhnedScraper, BbcScraper

logger = logging.getLogger(__name__)
SCRAPERS = [IdnesScraper(), IhnedScraper(), BbcScraper()]
job_command = f"cd {str(Path().resolve())}; pipenv run python -m app.scraper"


def scrape_news():
    """Gets articles from news servers and saves new ones into our DB.
    If any scraper fails, it must be logged but continue in operation.
    """
    max_workers = min(len(SCRAPERS), 10)
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        for scraper in SCRAPERS:
            executor.submit(_scrape_news, scraper)


def _scrape_news(scraper):
    """Helper function for scrape_news - processes a single scraper."""
    logger.info(f"Scraping news using {type(scraper).__name__}")
    try:
        articles = scraper.get_headers()
    except RequestException as exception:
        logger.error(exception)
    else:
        for article in articles:
            app.service.save_article_if_new(article)


def get_job(crontab: CronTab, command: str) -> CronItem or None:
    """Returns a job from crontab that matches the supplied command."""
    for job in crontab:
        if job.command == command:
            return job
    return None


if __name__ == '__main__':
    if not Path.exists(Path('logs')):
        Path.mkdir(Path('logs'))
    logging.basicConfig(level=logging.INFO, format='{asctime} {levelname:<8} {name}:{module}:{lineno} - {message}',
                        style='{')
    logger.addHandler(RotatingFileHandler(f'logs/scraper.log', maxBytes=10240, backupCount=10, encoding='utf-8'))

    logger.info("Scraper started.")
    scrape_news()

    if platform.system() != 'Linux':
        logger.warning("Scraper job was not scheduled. Only Linux platform is supported for scheduling.")
        sys.exit()

    cron = CronTab(user=True)
    if not get_job(cron, job_command):
        logger.info("Adding scraper job to crontab. Run 'python -m app.disable_scraper' to remove.")
        job = cron.new(command=job_command, comment="homework news scraper")
        job.minute.every(1)
        cron.write()
